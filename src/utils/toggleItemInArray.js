import { indexOf, without } from 'lodash';

export default function toggleItemInArray(collection, item) {
  const index = indexOf(collection, item);
  if (index !== -1) {
    return without(collection, item);
  }
  return [...collection, item];
}