import { SELECT_CORE, TOGGLE_FEATURE } from '../actions/types';
import toggleItemInArray from '../utils/toggleItemInArray'

export default function bookmarksReducer(state = [], action) {
  switch (action.type) {
    case SELECT_CORE:

    	return Object.assign({}, state, {
    		selected: {
    		 ...state.selected,
    		 'Core': action.payload.ID
    		}
    	})

    case TOGGLE_FEATURE:

		return Object.assign({}, state, {
    		selected: {
				...state.selected,
				'Features': toggleItemInArray(state.selected.Features, action.payload.ID)
    		}
    	})

    default:
      return state;
  }
}