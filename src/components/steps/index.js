import React, {Component} from 'react';

import StepOne from './step_one';
import StepTwo from './step_two';


class Steps extends Component{

	render(){
		return(
			<div>
				<StepOne></StepOne>
				<StepTwo></StepTwo>
			</div>
		)
	}

}

export default Steps;