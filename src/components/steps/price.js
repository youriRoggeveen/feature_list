import React from 'react'

const Price = ({price}) => {
	const text = price === 0 ? 'Free' : `+ $${price}/Mo`;

	return (
		<div className="price">
			{text}
		</div>
	)
}

export default Price