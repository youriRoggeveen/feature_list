import React from 'react';
import Price from './price';

const Feature = ({props, selected, onToggle}) =>{
	var buttonAttr = {}
	if(selected.includes(props.ID)){
		buttonAttr = {
			class: 'btn-selected',
			text: 'Added'
		}
	} else{
		buttonAttr = {
			class: 'btn-success',
			text: 'Add'
		}
	}

	return(
		<div className={`feature ${selected.includes(props.ID) ? 'selected' : ''}`}>
			<div className="feature-icon">
			<i className={`fi fi-${props.icon}`}></i>
			</div>
			<div className="feature-info">
				<h3>{props.title}</h3>
				<p>{props.description}</p>
			</div>
			<div className="feature-pricing">
				<Price price={props.price} />
				<button
					className={`btn btn-sm ${buttonAttr.class}`}
					onClick={() => onToggle(props.ID)}
				>
					{buttonAttr.text}
				</button>
			</div>
		</div>
	)
}

export default Feature;