import React from 'react';

const Core = ({props, callBack, index, selected}) => {
	return (
		<div className="col-6">
			<button
				className={`core btn btn-core ${selected === index ? 'selected' : ''}`}
				onClick={() => callBack(parseInt(`${index}`, 0))}
			>
				<h3 className="core-title">{props.title}</h3>
				<div className="core-info">
					<p className="core-description">{props.desc}</p>
					<p className="core-cap">under {props.maxPeople} people</p>
				</div>
				<p className="core-pricing">
					starting at <span className="price">${props.price}</span>
				</p>
			</button>
		</div>
	)
}

export default Core;