import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectCore } from '../../actions';
import StepHeading from './stepHeading';
import Core from './core';

class StepOne extends Component {
    render() {
        return (
        	<div className="step">
        		<StepHeading
        			title="customize your core"
        			number="1"
        			desc="your core seamlessly connects all your data with KIZEN's powerfull automation and insight engines"
        		/>
        		<div className="row">
					{this.props.cores.map((core) =>
						<Core
							key={core.ID}
							callBack={this.props.onSelect}
							props={core}
							index={core.ID}
							selected={this.props.selected}
						/>
					)}
				</div>
        	</div>
        );
    }
}

const mapStateToProps = state => {
  return {
		cores: state.cores,
		selected: state.selected.Core
  };
};

const mapDispatchToProps = dispatch => {
	return {
		onSelect: ID => {
			dispatch(selectCore(ID));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StepOne);