import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleFeature } from '../../actions';

import StepHeading from './stepHeading';
import Feature from './feature';

class StepTwo extends Component {
    render() {
        return (
   			<div className="step">
        		<StepHeading
	        		number="2"
        			title="Add features"
        			desc="Features can be added and removed as needed by your team."
        		/>
        		{this.props.features.map((feature, key) =>
        			<Feature
						key={key}
						props={feature}
						selected={this.props.selected}
						onToggle={this.props.onToggle}
        			/>
        			)}
        	</div>
        );
    }
}

const mapStateToProps = state => {
	return {
		features: state.features,
		selected: state.selected.Features
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onToggle: ID => {
			dispatch(toggleFeature(ID));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StepTwo);