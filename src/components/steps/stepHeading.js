import React from 'react';

const StepHeading = ({ number, title, desc }) =>
	<div className="stepHeading">
		<div className="step-number">Step {number}</div>
		<h2 className="step-title">{title}</h2>
		<p className="step-desc">{desc}</p>
	</div>

export default StepHeading;