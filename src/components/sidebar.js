import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {toggleFeature} from '../actions'
import Price from './steps/price';

const Selection = ({core, features, onToggle}) => {
	return (
		<div className="selection">
			<Package {...core[0]}/>
			{features.map((feature, key) => <Package key={key} onToggle={onToggle} {...feature} />)}
		</div>
	)
}

const Package = ({ID, title, price, addOn, onToggle}) =>{
	return (
		<div className="selected">
			<div className="selected-heading">
				<h4 className="selected-title">{title}</h4>
				{addOn ?
					<Fragment>
						<span className="addon">ADD ON</span>
						<i className="fi fi-close" onClick={() => onToggle(ID)}></i>
					</Fragment>
				: ''}
			</div>
			<Price price={price} />
		</div>
	)
}

class Sidebar extends Component{
	render(){
		return(
			<div className="sidebar">
				<div className="sidebar-heading">You've selected</div>
				<Selection {...this.props.selected} onToggle={this.props.onToggle}/>
				<p className="investment">Total investment</p>
				<div className="investment_price">
					{this.props.investment}$/mo
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		selected:{
			core: state.cores.filter( core => core.ID === state.selected.Core )
					.map(core => {return {
						ID: core.ID,
						title: core.title,
						price: core.price
					}}),
			features: state.features.filter( feature => state.selected.Features.includes(feature.ID) )
				.map(feature => {return {
						ID: feature.ID,
						title: feature.title,
						price: feature.price,
						addOn: true
					}})
		}
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		onToggle: ID => {
			dispatch(toggleFeature(ID));
		},
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);