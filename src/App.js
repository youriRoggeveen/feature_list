import React, { Component, Fragment } from 'react';
import Header from './components/header';
import Sidebar from './components/sidebar';
import Steps from './components/steps';

class App extends Component {
  render() {
    return (
		<Fragment>
		<Header />
		<div className="container">
			<div className="row">
				<div className="col-7">
					<Steps />
				</div>
				<div className="col-4 offset-1">
					<Sidebar />
				</div>
			</div>
		</div>
		</Fragment>
    );
  }
}

export default App;