import { SELECT_CORE, TOGGLE_FEATURE } from './types';

export const selectCore = ID => ({
	type: SELECT_CORE,
	payload: {
		type: 'core',
		ID
	}
})

export const toggleFeature = ID => ({
	type: TOGGLE_FEATURE,
	payload: {
		type: 'feature',
		ID
	}
})