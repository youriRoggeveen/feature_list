export const initialStore = {
	cores:
		[{
			ID: 0,
			title: 'Growth Edition',
			desc: 'the best technology for teams',
			maxPeople: 100,
			price: 50
		},
		{
			ID: 1,
			title: 'Enterprice Edition',
			desc: 'Advanced AI for teams',
			maxPeople: 100,
			price: 18000
		}],
	features:
		[{
			ID: 0,
			title: 'Emails',
			'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit cumque ducimus nobis itaque fugit .Et, vel!',
			price: 60,
			icon: 'mail'
		},
		{
			ID: 1,
			title: 'Forms',
			'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit cumque ducimus nobis itaque fugit .Et, vel!',
			price: 0,
			icon: 'check-form'
		},
		{
			ID: 2,
			title: 'Activities',
			'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit cumque ducimus nobis itaque fugit .Et, vel!',
			price: 10,
			icon: 'graph'
		},
		{
			ID: 3,
			title: 'Ads',
			'description': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit cumque ducimus nobis itaque fugit .Et, vel!',
			price: 5,
			icon: 'love-and-romance'
		}],
		selected: {
			Core: 0,
			Features: []
		}
	}