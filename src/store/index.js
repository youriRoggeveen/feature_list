import { createStore } from 'redux';
import rootReducer from '../reducers';

import { initialStore } from './initialStore'

export const store = createStore(
	rootReducer,
	initialStore,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);